﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Peliculas.Entries;
using Peliculas.Entries.DTOs;

namespace Peliculas.Services.Interfaces
{
    public interface IGenericServices<T>  where T : EntityBaseClass
    {
        Task<IList<T>> GetAllData();
        IQueryable<T> GetAll();
        Task<T> GetById(int id);
        Task<T> Add(T item);
        Task<T> Update(T item);
        Task Remove(int id);
    }
}
