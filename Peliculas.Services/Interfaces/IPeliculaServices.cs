﻿using System;
using System.Threading.Tasks;
using Peliculas.Entries;
using Peliculas.Entries.Helpers;

namespace Peliculas.Services.Interfaces
{
    public interface IPeliculaServices : IGenericServices<Movie>
    {
        void EscribirOrdenActores(Movie pelicula);
        Task<Movie> GetPeliculaById(int id);
        Task<Movie> GetPeliculaToUpdateById(int id);
        Task<EstrenoEncine> GetAllMovies();


    }
}
