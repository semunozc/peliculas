﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Peliculas.Entries;

namespace Peliculas.Services.Interfaces
{
    public interface IGenreServices : IGenericServices<Genre>
    {
        Task<List<Genre>> GetGeneroNoSeleccionado(List<int> generosSeleccionadosIds);
    }
}
