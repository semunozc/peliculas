﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Peliculas.Entries;

namespace Peliculas.Services.Interfaces
{
    public interface ICineServices : IGenericServices<Cine>
    {
        Task<List<Cine>> GetCineNoSeleccionado(List<int> cinesNoSeleccionados);
    }
}
