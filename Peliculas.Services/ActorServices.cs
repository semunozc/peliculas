﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Pelicula.Data.Interfaces;
using Peliculas.Entries;
using Peliculas.Entries.DTOs;
using Peliculas.Services.Interfaces;
namespace Peliculas.Services
{
    public class ActorServices : GenericService<Actor>, IActorServices
    {
        private IActorRepository _actorRepository;
        public ActorServices(IActorRepository actorRepostory) : base(actorRepostory)
        {
            _actorRepository = actorRepostory;
        }

        public Task<List<PeliculaActorDTO>> GetByName(string Nombre)
        {
            return _actorRepository.GetByName(Nombre);
        }
    }
}
