﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Pelicula.Data.Interfaces;
using Peliculas.Entries;
using Peliculas.Services.Interfaces;

namespace Peliculas.Services
{
    public class CineServices : GenericService<Cine>, ICineServices
    {
        private ICineRepository _cineRepository;
        public CineServices(ICineRepository cineRepostory) : base(cineRepostory)
        {
            _cineRepository = cineRepostory;
        }

        public async Task<List<Cine>> GetCineNoSeleccionado(List<int> cinesSeleccionadosIds)
        {
            return await _cineRepository.GetCineNoSeleccionado(cinesSeleccionadosIds);
        }
    }
}
