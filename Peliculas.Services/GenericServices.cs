﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Pelicula.Data.Interfaces;
using Peliculas.Entries;
using Peliculas.Entries.DTOs;
using Peliculas.Services.Interfaces;

namespace Peliculas.Services
{
    public class GenericService<T> : IGenericServices<T> where T : EntityBaseClass
    {
        private IGenericRepository<T> _genericRepostory;

        public GenericService(IGenericRepository<T> genericRepostory)
        {
            _genericRepostory = genericRepostory;
        }

        public async Task<T> Add(T item)
        {
            return await _genericRepostory.Add(item);
        }

        public IQueryable<T> GetAll()
        {
            return _genericRepostory.GetAll();

        }

        public async Task<IList<T>> GetAllData()
        {
           return await _genericRepostory.GetAllData();
        }

        public async Task<T> GetById(int id)
        {
          return  await _genericRepostory.GetById(id);

        }


        public async Task Remove(int id)
        {
           await _genericRepostory.Remove(id);

        }

        public async Task<T> Update(T item)
        {
            return await _genericRepostory.Update(item);
        }
    }
}
