﻿using System;
using System.Collections.Generic;

namespace Peliculas.Entries.Helpers
{
    public class EstrenoEncine
    {
        public List<Movie> PoximosEstrenos { get; set; }
        public List<Movie> EnCines { get; set; }
    }
}
