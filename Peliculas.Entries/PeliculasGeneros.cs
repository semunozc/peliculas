﻿using System;
namespace Peliculas.Entries
{
    public class PeliculasGeneros
    {
        public int PeliculaId { get; set; }
        public int GeneroId { get; set; }
        public Movie Pelicula { get; set; }
        public Genre Genero { get; set; }
    }
}
