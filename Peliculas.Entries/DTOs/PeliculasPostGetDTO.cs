﻿using System;
using System.Collections.Generic;

namespace Peliculas.Entries.DTOs
{
    public class PeliculasPostGetDTO
    {
        public List<GenreDTO> Generos { get; set; }
        public List<CineDTO> Cines { get; set; }
    }
}
