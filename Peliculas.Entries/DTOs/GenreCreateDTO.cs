﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Peliculas.Entries.DTOs
{
    public class GenreCreateDTO
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "El campo {0} es requerido")]
        [StringLength(maximumLength: 50, ErrorMessage = "El limite de caracteres del campo {0} debe ser de 50")]
        public string Nombre { get; set; }
    }
}
