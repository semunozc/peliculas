﻿using System;
namespace Peliculas.Entries.DTOs
{
    public class GenreDTO
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}
