﻿using System;
namespace Peliculas.Entries.DTOs
{
    public class PeliculasFiltrarDTO
    {
        public int Pagina { get; set; }
        public int RecordsPorPagina { get; set; }
        public PaginationDTO PaginacionDTO
        {
            get { return new PaginationDTO() { Page = Pagina, RecordsPerPages = RecordsPorPagina }; }
        }
        public string Titulo { get; set; }
        public int GeneroId { get; set; }
        public bool EnCines { get; set; }
        public bool ProximosEstrenos { get; set; }
    }
}
