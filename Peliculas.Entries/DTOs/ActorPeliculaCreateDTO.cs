﻿using System;
namespace Peliculas.Entries.DTOs
{
    public class ActorPeliculaCreateDTO
    {
        public int Id { get; set; }
        public string Personaje { get; set; }
    }
}
