﻿using System;
namespace Peliculas.Entries.DTOs
{
    public class PaginationDTO
    {
        public int Page { get; set; } = 1;
        //private int _recordsPerPages { get; set; } = 10;
        private int recordsPorPagina = 10;

        private readonly int MaxRecordsPerPages = 50;

        public int RecordsPerPages
        {
            get
            {
                return recordsPorPagina;
            }
            set
            {
                recordsPorPagina = (value > MaxRecordsPerPages) ? MaxRecordsPerPages : value;
            }
        }
    }
}
