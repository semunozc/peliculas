﻿using System;
namespace Peliculas.Entries
{
    public class PeliculasCines
    {
        public int PeliculaId { get; set; }
        public int CineId { get; set; }
        public Movie Pelicula { get; set; }
        public Cine Cine { get; set; }
    }
}
