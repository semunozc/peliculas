﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Peliculas.Entries
{
    public class CredencialesUsuario
    {
        [EmailAddress]
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
