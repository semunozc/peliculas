using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using NetTopologySuite;
using NetTopologySuite.Geometries;
using Pelicula.Data.Data;
using Pelicula.Data.Interfaces;
using Pelicula.Data.Repositories;
using Peliculas.Api.ApiBehavior;
using Peliculas.Api.Filters;
using Peliculas.Api.Helpers;
using Peliculas.Entries;
using Peliculas.Services;
using Peliculas.Services.Interfaces;

namespace Peliculas.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddIdentity<ApplicationUser, IdentityRole>().AddEntityFrameworkStores<ApplicationDbContext>().AddDefaultTokenProviders();
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(opciones =>
            opciones.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = false,
                ValidateAudience = false,
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtKey"])),
                ClockSkew = TimeSpan.Zero
            });

            services.AddControllers(options => {

                options.Filters.Add(typeof(ParseBadRequest));
            }).ConfigureApiBehaviorOptions(BehaviorBadRequest.Parses);
            services.AddCors(options =>
            {
                var frontendURL = Configuration.GetValue<string>("frontend_url");
                options.AddDefaultPolicy(builder =>
                {
                    builder.WithOrigins(frontendURL).AllowAnyMethod().AllowAnyHeader().WithExposedHeaders(new string[] { "entries" });
                });
            });
            services.AddAutoMapper(typeof(Startup));

            services.AddSingleton(provider =>
                       new MapperConfiguration(config =>
                       {
                           var geometryFactory = provider.GetRequiredService<GeometryFactory>();
                           config.AddProfile(new AutoMapperProfiles(geometryFactory));
                       }).CreateMapper());


            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"),
                sqlServer => sqlServer.UseNetTopologySuite()));

            services.AddSingleton<GeometryFactory>(NtsGeometryServices.Instance.CreateGeometryFactory(srid: 4326));

            services.AddTransient<IAlmacenadorArchivos, AlmacenadorArchivosLocal>();
            services.AddHttpContextAccessor();

            services.AddScoped<IGenreRepository, GenreRepository>();
            services.AddScoped<IGenreServices, GenreServices>();
            services.AddScoped<IActorRepository, ActorsRepository>();
            services.AddScoped<IActorServices, ActorServices>();
            services.AddScoped<ICineRepository, CinesRepository>();
            services.AddScoped<ICineServices, CineServices>();
            services.AddScoped<IPeliculaRepository, PeliculasRepository>();
            services.AddScoped<IPeliculaServices, PeliculaServices>();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseCors();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }
    }
}
