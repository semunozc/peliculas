﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace Peliculas.Api.ApiBehavior
{
    public static class BehaviorBadRequest
    {

        public static void Parses(ApiBehaviorOptions options)
        {
            options.InvalidModelStateResponseFactory = actionContext =>
            {
                var response = new List<string>();
                foreach (var keys in actionContext.ModelState.Keys)
                {
                    foreach (var error in actionContext.ModelState[keys].Errors)
                    {
                        response.Add(error.ErrorMessage);
                    }
                }
                return new BadRequestObjectResult(response);
            };
        }
         
    }
}
