﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Peliculas.Api.Helpers;
using Peliculas.Entries;
using Peliculas.Entries.DTOs;
using Peliculas.Services.Interfaces;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Peliculas.Api.Controllers
{
    [Route("api/peliculas")]
    [ApiController]
    public class PeliculasController : ControllerBase
    {
        private readonly IPeliculaServices _peliculaServ;
        private readonly IGenreServices _genreServ;
        private readonly ICineServices _cineServ;
        private readonly IMapper _mapper;
        private readonly IAlmacenadorArchivos _almacenadorArchivos;
        private readonly string contenedor = "peliculas";

        public PeliculasController(IPeliculaServices peliculaServ,
            IGenreServices genreServ,
            ICineServices cineServ,
            IMapper maper,
            IAlmacenadorArchivos almacenadorArchivos)
        {
            _peliculaServ = peliculaServ;
            _genreServ = genreServ;
            _cineServ = cineServ;
            _mapper = maper;
            _almacenadorArchivos = almacenadorArchivos;
        }

        [HttpGet]
        public async Task<ActionResult<LandingPageDTO>> Get()
        {
            var result = await _peliculaServ.GetAllMovies();

            LandingPageDTO resultado = new()
            {
                ProximosEstrenos = _mapper.Map<List<PeliculaDTO>>(result.PoximosEstrenos),
                EnCines = _mapper.Map<List<PeliculaDTO>>(result.EnCines)
            };

            return resultado;
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<PeliculaDTO>> Get(int id)
        {
            var pelicula = await _peliculaServ.GetPeliculaById(id);

            if (pelicula == null) { return NotFound(); }

            var dto = _mapper.Map<PeliculaDTO>(pelicula);
            dto.Actores = dto.Actores.OrderBy(x => x.Orden).ToList();
            return dto;
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromForm] PeliculaCreateDTO peliculaCreateDTO)
        {
            var pelicula = _mapper.Map<Movie>(peliculaCreateDTO);

            if (peliculaCreateDTO.Poster != null)
            {
                pelicula.Poster = await _almacenadorArchivos.GuardarArchivo(contenedor, peliculaCreateDTO.Poster);
            }
            _peliculaServ.EscribirOrdenActores(pelicula);

            await _peliculaServ.Add(pelicula);

            return NoContent();

        }

        [HttpGet("PostGet")]
        public async Task<ActionResult<PeliculasPostGetDTO>> PostGet()
        {
            var cines = await _cineServ.GetAll().ToListAsync();
            var generos = await _genreServ.GetAll().ToListAsync();

            var cinesDTO = _mapper.Map<List<CineDTO>>(cines);
            var generosDTO = _mapper.Map<List<GenreDTO>>(generos);

            return new PeliculasPostGetDTO() { Cines = cinesDTO, Generos = generosDTO };
        }

        [HttpGet("PutGet/{id:int}")]
        public async Task<ActionResult<PeliculasPutGetDTO>> PutGet(int id)
        {
            var peliculaActionResult = await Get(id);
            if (peliculaActionResult.Result is NotFoundResult) { return NotFound(); }

            var pelicula = peliculaActionResult.Value;

            var generosSeleccionadosIds = pelicula.Generos.Select(x => x.Id).ToList();

            var generosNoSeleccionados = await _genreServ.GetGeneroNoSeleccionado(generosSeleccionadosIds);

            var cinesSeleccionadosIds = pelicula.Cines.Select(x => x.Id).ToList();

            var cinesNoSeleccionados = await _cineServ.GetCineNoSeleccionado(generosSeleccionadosIds);

            var generosNoSeleccionadosDTO = _mapper.Map<List<GenreDTO>>(generosNoSeleccionados);
            var cinesNoSeleccionadosDTO = _mapper.Map<List<CineDTO>>(cinesNoSeleccionados);

            var respuesta = new PeliculasPutGetDTO();
            respuesta.Pelicula = pelicula;
            respuesta.GenerosSeleccionados = pelicula.Generos;
            respuesta.GenerosNoSeleccionados = generosNoSeleccionadosDTO;
            respuesta.CinesSeleccionados = pelicula.Cines;
            respuesta.CinesNoSeleccionados = cinesNoSeleccionadosDTO;
            respuesta.Actores = pelicula.Actores;
            return respuesta;
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult> Put(int id, [FromForm] PeliculaCreateDTO peliculaCreacionDTO)
        {
            var pelicula = await _peliculaServ.GetPeliculaToUpdateById(id);

            if (pelicula == null)
            {
                return NotFound();
            }

            pelicula = _mapper.Map(peliculaCreacionDTO, pelicula);

            if (peliculaCreacionDTO.Poster != null)
            {
                pelicula.Poster = await _almacenadorArchivos.EditarArchivo(contenedor, peliculaCreacionDTO.Poster, pelicula.Poster);
            }

            _peliculaServ.EscribirOrdenActores(pelicula);

            await _peliculaServ.Update(pelicula);
            return NoContent();
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete(int id)
        {
            var pelicula = await _peliculaServ.GetById(id);

            if (pelicula == null)
            {
                return NotFound();
            }

            await _peliculaServ.Remove(pelicula.Id);
            await _almacenadorArchivos.BorrarArchivo(pelicula.Poster, contenedor);
            return NoContent();
        }

        [HttpGet("filtrar")]
        public async Task<ActionResult<List<PeliculaDTO>>> Filtrar([FromQuery] PeliculasFiltrarDTO peliculasFiltrarDTO)
        {
            var peliculasQueryable = _peliculaServ.GetAll();

            if (!string.IsNullOrEmpty(peliculasFiltrarDTO.Titulo))
            {
                peliculasQueryable = peliculasQueryable.Where(x => x.Titulo.Contains(peliculasFiltrarDTO.Titulo));
            }

            if (peliculasFiltrarDTO.EnCines)
            {
                peliculasQueryable = peliculasQueryable.Where(x => x.EnCines);
            }

            if (peliculasFiltrarDTO.ProximosEstrenos)
            {
                var hoy = DateTime.Today;
                peliculasQueryable = peliculasQueryable.Where(x => x.FechaLanzamiento > hoy);
            }

            if (peliculasFiltrarDTO.GeneroId != 0)
            {
                peliculasQueryable = peliculasQueryable
                    .Where(x => x.PeliculasGeneros.Select(y => y.GeneroId)
                    .Contains(peliculasFiltrarDTO.GeneroId));
            }

            await HttpContext.InsertParamsPageInHeader(peliculasQueryable);

            var peliculas = await peliculasQueryable.Paginetion(peliculasFiltrarDTO.PaginacionDTO).ToListAsync();
            return _mapper.Map<List<PeliculaDTO>>(peliculas);
        }
    }
}
