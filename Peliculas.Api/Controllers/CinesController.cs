﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Peliculas.Api.Helpers;
using Peliculas.Entries;
using Peliculas.Entries.DTOs;
using Peliculas.Services.Interfaces;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Peliculas.Api.Controllers
{
    [Route("api/cines")]
    [ApiController]
    public class CinesController : ControllerBase
    {
        private readonly ICineServices _cineServ;
        private readonly IMapper _mapper;

        public CinesController(ICineServices cineServ, IMapper maper)
        {
            _cineServ = cineServ;
            _mapper = maper;
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody] CineCreateDTO cineCreateDTO)
        {
            var cine = _mapper.Map<Cine>(cineCreateDTO);
            await _cineServ.Add(cine);
            return NoContent();
        }

        [HttpGet]
        public async Task<ActionResult<List<CineDTO>>> Get([FromQuery] PaginationDTO paginationDTO)
        {
            var queryable = _cineServ.GetAll();
            await HttpContext.InsertParamsPageInHeader(queryable);
            var cines = await queryable.OrderBy(x => x.Nombre).Paginetion(paginationDTO).ToListAsync();
            return _mapper.Map<List<CineDTO>>(cines);
        }


        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete(int id)
        {
            var existeCine = await _cineServ.GetById(id);
            if (existeCine == null)
            {
                return NotFound();
            }
            await _cineServ.Remove(existeCine.Id);

            return NoContent();
        }


        [HttpGet("{Id:int}")]
        public async Task<ActionResult<CineDTO>> Get(int Id)
        {
            var cine = await _cineServ.GetById(Id);
            if (cine == null)
            {
                return NotFound();
            }
            return _mapper.Map<CineDTO>(cine);
        }

        [HttpPut("{Id:int}")]
        public async Task<ActionResult> Put(int Id, [FromBody] CineCreateDTO  cineCreateDTO)
        {
            var cine = await _cineServ.GetById(Id);
            if (cine == null)
            {
                return NotFound();
            }
            cine = _mapper.Map(cineCreateDTO, cine);
            await _cineServ.Update(cine);
            return NoContent();
        }

    }
}
