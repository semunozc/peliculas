﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Peliculas.Api.Helpers;
using Peliculas.Entries;
using Peliculas.Entries.DTOs;
using Peliculas.Services.Interfaces;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Peliculas.Api.Controllers
{
    [Route("api/actores")]
    [ApiController]
    public class ActorsController : ControllerBase
    {
        private readonly IActorServices _actorServ;
        private readonly IMapper _mapper;
        private readonly IAlmacenadorArchivos _almacenadorArchivos;
        private readonly string contenedor = "actores";

        public ActorsController(IActorServices actorServ, IMapper maper, IAlmacenadorArchivos almacenadorArchivos)
        {
            _actorServ = actorServ;
            _mapper = maper;
            _almacenadorArchivos = almacenadorArchivos;
        }


        [HttpGet("buscarPorNombre/{nombre}")]
        public async Task<ActionResult<List<PeliculaActorDTO>>> BuscarPorNombre(string nombre = "")
        {
            if (string.IsNullOrWhiteSpace(nombre)) { return new List<PeliculaActorDTO>();}
            return await _actorServ.GetByName(nombre);
        }

        //Agregar
        [HttpPost]
        public async Task<ActionResult> Post([FromForm] ActorCreateDTO actorCreateDTO)
        {
            var actor = _mapper.Map<Actor>(actorCreateDTO);

            if (actorCreateDTO.Foto != null)
            {
                actor.Foto = await _almacenadorArchivos.GuardarArchivo(contenedor, actorCreateDTO.Foto);
            }
            await _actorServ.Add(actor);

            return NoContent();

        }

        [HttpGet("{Id:int}")]
        public async Task<ActionResult<ActorDTO>> Get(int Id)
        {
            var actor = await _actorServ.GetById(Id);
            if (actor == null)
            {
                return NotFound();
            }
            return _mapper.Map<ActorDTO>(actor);
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult> Put(int id, [FromForm] ActorCreateDTO actorCreateDTO)
        {
            var actor = await _actorServ.GetById(id);

            if (actor == null)
            {
                return NotFound();
            }

            actor = _mapper.Map(actorCreateDTO, actor);

            if (actorCreateDTO.Foto != null)
            {
                actor.Foto = await _almacenadorArchivos.EditarArchivo(contenedor, actorCreateDTO.Foto, actor.Foto);
            }

            await _actorServ.Update(actor);
            return NoContent();

        }

        [HttpGet]
        public async Task<ActionResult<List<ActorDTO>>> Get([FromQuery] PaginationDTO paginationDTO)
        {
            var queryable = _actorServ.GetAll();
            await HttpContext.InsertParamsPageInHeader(queryable);
            var actores = await queryable.OrderBy(x => x.Nombre).Paginetion(paginationDTO).ToListAsync();
            return _mapper.Map<List<ActorDTO>>(actores);
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> Delete(int id)
        {
            var existeActor = await _actorServ.GetById(id);
            if (existeActor == null)
            {
                return NotFound();
            }
            await _actorServ.Remove(existeActor.Id);

            return NoContent();
        }
    }
}
