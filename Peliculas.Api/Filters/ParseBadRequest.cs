﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Infrastructure;

namespace Peliculas.Api.Filters
{
    public class ParseBadRequest : IActionFilter
    {

        public void OnActionExecuted(ActionExecutedContext context)
        {
            var castResul = context.Result as IStatusCodeActionResult;
            if (castResul == null)
            {
                return;
            }
            var statusCode = castResul.StatusCode;
            if (statusCode == 400)
            {
                var response = new List<string>();
                var ActualResult = context.Result as BadRequestObjectResult;
                if (ActualResult.Value is string)
                {
                    response.Add(((string)ActualResult.Value).ToString());
                }
                else if (ActualResult.Value is IEnumerable<IdentityError> errors)
                {
                    foreach (var error in errors)
                    {
                        response.Add(error.Description);
                    }
                }
                else
                {
                    foreach (var keys in context.ModelState.Keys)
                    {
                        foreach (var error in context.ModelState[keys].Errors)
                        {
                            response.Add(error.ErrorMessage);
                        }
                    }
                }
                context.Result = new BadRequestObjectResult(response);
;            }
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {

        }
    }
}
