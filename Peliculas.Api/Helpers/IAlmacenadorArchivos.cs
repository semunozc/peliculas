﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Peliculas.Api.Helpers
{
    public interface IAlmacenadorArchivos
    {
        Task BorrarArchivo(string ruta, string contenedor);
        Task<string> EditarArchivo(string contenedor, IFormFile archivo, string ruta);
        Task<string> GuardarArchivo(string contenedor, IFormFile archivo);
    }
}
