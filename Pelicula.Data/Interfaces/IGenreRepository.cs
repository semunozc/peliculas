﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Peliculas.Entries;

namespace Pelicula.Data.Interfaces
{
    public interface IGenreRepository : IGenericRepository<Genre>
    {
        Task<List<Genre>> GetGeneroNoSeleccionado(List<int> generosSeleccionadosIds);
    }
}
