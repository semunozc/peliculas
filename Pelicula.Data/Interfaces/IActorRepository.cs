﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Peliculas.Entries;
using Peliculas.Entries.DTOs;

namespace Pelicula.Data.Interfaces
{
    public interface IActorRepository : IGenericRepository<Actor>
    {
        Task<List<PeliculaActorDTO>> GetByName(string Nombre);
    }
}
