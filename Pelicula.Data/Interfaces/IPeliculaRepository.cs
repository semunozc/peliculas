﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Peliculas.Entries;
using Peliculas.Entries.DTOs;
using Peliculas.Entries.Helpers;

namespace Pelicula.Data.Interfaces
{
    public interface IPeliculaRepository : IGenericRepository<Movie>
    {
        Task<Movie> GetPeliculaById(int id);
        Task<Movie> GetPeliculaToUpdateById(int id);
        Task<EstrenoEncine> GetAllMovies();
    }
}
