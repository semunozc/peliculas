﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Pelicula.Data.Migrations
{
    public partial class newGenero : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Genres",
                newName: "Nombre");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Nombre",
                table: "Genres",
                newName: "Name");
        }
    }
}
