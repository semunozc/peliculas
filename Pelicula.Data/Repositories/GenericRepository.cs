﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Pelicula.Data.Data;
using Pelicula.Data.Interfaces;
using Peliculas.Entries;
using Peliculas.Entries.DTOs;

namespace Pelicula.Data.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : EntityBaseClass
    {
        protected readonly ApplicationDbContext _context;

        public GenericRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IList<T>> GetAllData()
        {
            return await _context.Set<T>().ToListAsync();
        }

        public async Task<T> GetById(int id)
        {
            return await _context.Set<T>().FindAsync(id);
        }

        public async Task Remove(int id)
        {
            var entity = await GetById(id);
            if (entity == null)
                throw new Exception("La propiedad es nula");

            _context.Set<T>().Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<T> Add(T item)
        {
            _context.Set<T>().Add(item);
             await _context.SaveChangesAsync();
            return item;
        }

        public async Task<T> Update(T item)
        {
            _context.Entry(item).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return item;
        }

        public IQueryable<T> GetAll()
        {
            return _context.Set<T>().AsQueryable();
        }
    }
}
