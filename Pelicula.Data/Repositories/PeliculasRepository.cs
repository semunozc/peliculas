﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Pelicula.Data.Data;
using Pelicula.Data.Interfaces;
using Peliculas.Entries;
using Peliculas.Entries.DTOs;
using Peliculas.Entries.Helpers;

namespace Pelicula.Data.Repositories
{
    public class PeliculasRepository : GenericRepository<Movie>, IPeliculaRepository
    {
        public PeliculasRepository(ApplicationDbContext context) : base(context)
        {
        }

        public async Task<EstrenoEncine> GetAllMovies()
        {
            int top = 6;
            DateTime hoy = DateTime.Today;

            var proximosEstrenos = await _context.Peliculas
                .Where(x => x.FechaLanzamiento > hoy)
                .OrderBy(x => x.FechaLanzamiento)
                .Take(top)
                .ToListAsync();

            var enCines = await _context.Peliculas
                .Where(x => x.EnCines)
                .OrderBy(x => x.FechaLanzamiento)
                .Take(top)
                .ToListAsync();

            EstrenoEncine estrenoEncine = new()
            {
                 EnCines = enCines,
                 PoximosEstrenos = proximosEstrenos
            };

            return estrenoEncine;
        }
 

        public async Task<Movie> GetPeliculaById(int id)
        {
            var pelicula = await _context.Peliculas
                    .Include(x => x.PeliculasGeneros).ThenInclude(x => x.Genero)
                    .Include(x => x.PeliculasActores).ThenInclude(x => x.Actor)
                    .Include(x => x.PeliculasCines).ThenInclude(x => x.Cine)
                    .FirstOrDefaultAsync(x => x.Id == id);
            return pelicula;
        }

        public async Task<Movie> GetPeliculaToUpdateById(int id)
        {
            var pelicula = await _context.Peliculas
                      .Include(x => x.PeliculasActores)
                      .Include(x => x.PeliculasGeneros)
                      .Include(x => x.PeliculasCines)
                      .FirstOrDefaultAsync(x => x.Id == id);
            return pelicula;
        }
    }
}
