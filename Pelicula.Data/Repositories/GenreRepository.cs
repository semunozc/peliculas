﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Pelicula.Data.Data;
using Pelicula.Data.Interfaces;
using Peliculas.Entries;

namespace Pelicula.Data.Repositories
{
    public class GenreRepository : GenericRepository<Genre>, IGenreRepository
    {

        public GenreRepository(ApplicationDbContext context) : base(context)
        {
        }

        public async Task<List<Genre>> GetGeneroNoSeleccionado(List<int> generosSeleccionadosIds)
        {
            return await _context.Genres.Where(x => !generosSeleccionadosIds.Contains(x.Id)).ToListAsync();
        }
    }
}
