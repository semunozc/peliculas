﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Pelicula.Data.Data;
using Pelicula.Data.Interfaces;
using Peliculas.Entries;
using Peliculas.Entries.DTOs;

namespace Pelicula.Data.Repositories
{
    public class ActorsRepository : GenericRepository<Actor>, IActorRepository
    {

        public ActorsRepository(ApplicationDbContext context) : base(context)
        {
        }

        public async Task<List<PeliculaActorDTO>> GetByName(string Nombre)
        {
            return await _context.Actors.Where(x => x.Nombre.Contains(Nombre)).OrderBy(x => x.Nombre).
                Select(x => new PeliculaActorDTO { Id = x.Id, Nombre = x.Nombre, Foto = x.Foto }).Take(5).ToListAsync();
        }
    }
}
